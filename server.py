#!/usr/bin/env python
import time
import socket

host = ''
port = 50000
backlog = 5
size = 1024
count = 0
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host,port))
s.listen(backlog)
while 1:
    client, address = s.accept()
    data = client.recv(size)
    if data:
        count = count + 1
        text_file = open(str(count) + ".txt", "w")
        text_file.write("done")
        text_file.close()
        time.sleep(0.1)
        print data
        client.send(data)
    client.close()

